(** {1 Dynarray benchmarks} 
   
    A benchmark for the various implementations of dynarray.

    Documentation comments throughout the code explain the user-facing
    interface. *)

(** {2 Benchmarks, configurations implementations.} *)

(* Supported benchmarks *)
type bench =
  | Get
  | Set
  | Add
  | Add_pop
  | Iter
  | To_array
  | Of_array
  | Of_list
  | Append_array

type impl =
  | Array
  | Stack
  | Queue
  | Base_stack
  | CCVector
  | Dynarray_boxed
  | Dynarray_unboxed

type conf = {
  rounds : int;
  (* number of iterations of the benchmark *)

  length : int;
  (* length of structures manipulated in the benchmark. *)
}


(** {2 Environment variables} *)

let int_of_env var =
  try int_of_string (Sys.getenv var)
  with _ ->
    Printf.ksprintf failwith
      "Expected a number for environment variable %s"
      var

let assoc_of_env var assoc =
  let fail () =
    Printf.ksprintf failwith
      "Expected environment variable %s in [%s]"
      var
      (String.concat " | " (List.map fst assoc))
  in
  try List.assoc (Sys.getenv var) assoc
  with _ -> fail ()

let rounds =
  int_of_env "ROUNDS"

let length =
  int_of_env "LENGTH"

let bench =
  assoc_of_env "BENCH" [
    "Get", Get;
    "Set", Set;
    "Add", Add;
    "Add_pop", Add_pop;
    "Iter", Iter;
    "To_array", To_array;
    "Of_array", Of_array;
    "Of_list", Of_list;
    "Append_array", Append_array;
  ]

let impl =
  assoc_of_env "IMPL" [
    "Array", Array;
    "Base_stack", Base_stack;
    "Stack", Stack;
    "Queue", Queue;
    "CCVector", CCVector;
    "Dynarray_boxed", Dynarray_boxed;
    "Dynarray_unboxed", Dynarray_unboxed;
  ]

let conf = {
  rounds;
  length;
}

let unsupported () =
  Printf.ksprintf failwith
    "Unsupported combination BENCH=%s IMPL=%s"
    (Sys.getenv "BENCH") (Sys.getenv "IMPL")


(** {2 Benchmark code}

    It would be natural to shoehorn all the modules we compare into
    a common interface (by writing adapter code when necessary), and
    then write our benchmark as a functor over that interface.

    The problem with that approach is that the fiability of the
    benchmark depends massively over whether the functor application
    and adapter code are properly inlined and simplified away. It may
    work correctly with flambda and some careful tuning (I've done it
    in the past for some projects), but it makes it impossible to test
    with less agressive compiler optimizations settings.

    This comes from the fact that some of the functions we want to
    test, in particular 'get', are very fast, so adding an indirect
    call strongly distorts the results.

    Instead the approach we use is to just duplicate the benchmark
    code for each implementation we want to test. This is very
    verbose, but reliable.
*)

let opaque = Sys.opaque_identity

module Bench_get = struct
  (* We use [@inline never] and repeat the name of the operation, here
     'get_', to ensure that it is easy to locate the function in
     the -dlambda or -dcmm outputs. *)

  let[@inline never] get_array conf =
    let a = Array.make conf.length (opaque None) in
    for _ = 1 to 10 * conf.rounds do
      for i = 0 to conf.length - 1 do
        let res = a.(i) in
        ignore (opaque res);
      done
    done

  let[@inline never] get_base_stack conf =
    let v = opaque None in
    let a =
      Base_stack.of_list (List.init conf.length (fun _ -> v)) in
    for _ = 1 to 10 * conf.rounds do
      for i = 0 to conf.length - 1 do
        let res = Base_stack.get a i in
        ignore (opaque res);
      done
    done    

  let[@inline never] get_ccvector conf =
    let a = CCVector.make conf.length (opaque None) in
    for _ = 1 to 10 * conf.rounds do
      for i = 0 to conf.length - 1 do
        let res = CCVector.get a i in
        ignore (opaque res);
      done
    done

  let[@inline never] get_dynarray_boxed conf =
    let a = Dynarray_boxed.make conf.length (opaque None) in
    for _ = 1 to 10 * conf.rounds do
      for i = 0 to conf.length - 1 do
        let res = Dynarray_boxed.get a i in
        ignore (opaque res);
      done
    done

  let[@inline never] get_dynarray_unboxed conf =
    let a = Dynarray_unboxed.make conf.length (opaque None) in
    for _ = 1 to 10 * conf.rounds do
      for i = 0 to conf.length - 1 do
        let res = Dynarray_unboxed.get a i in
        ignore (opaque res);
      done
    done
end



module Bench_set = struct
  let[@inline never] set_array conf =
    let a = Array.make conf.length (opaque None) in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for i = 0 to conf.length - 1 do
        a.(i) <- opaque v;
      done
    done

  let[@inline never] set_base_stack conf =
    let v = opaque None in
    let a =
      Base_stack.of_list (List.init conf.length (fun _ -> v)) in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for i = 0 to conf.length - 1 do
        let res = Base_stack.set a i v in
        ignore (opaque res);
      done
    done    

  let[@inline never] set_ccvector conf =
    let a = CCVector.make conf.length (opaque None) in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for i = 0 to conf.length - 1 do
        CCVector.set a i v;
      done
    done

  let[@inline never] set_dynarray_boxed conf =
    let a = Dynarray_boxed.make conf.length (opaque None) in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for i = 0 to conf.length - 1 do
        Dynarray_boxed.set a i v;
      done
    done

  let[@inline never] set_dynarray_unboxed conf =
    let a = Dynarray_unboxed.make conf.length (opaque None) in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for i = 0 to conf.length - 1 do
        Dynarray_unboxed.set a i v;
      done
    done
end


module Bench_add = struct
  (* In this benchmark, we create a fresh container on each round.
     The cost of growing the container will be paid each round instead
     of being amortized.  This is representative of workloads that use
     Dynarray as a buffer data structure, and convert it to another
     representation right away. *)
  let[@inline never] add_stack conf =
    for r = 1 to conf.rounds do
      let a = Stack.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Stack.push v a;
      done;
    done

  let[@inline never] add_base_stack conf =
    for r = 1 to conf.rounds do
      let a = Base_stack.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Base_stack.push a v;
      done;
    done

  let[@inline never] add_queue conf =
    for r = 1 to conf.rounds do
      let a = Queue.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Queue.push v a;
      done;
    done

  let[@inline never] add_ccvector conf =
    for r = 1 to conf.rounds do
      let a = CCVector.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        CCVector.push a v;
      done;
    done

  let[@inline never] add_dynarray_boxed conf =
    for r = 1 to conf.rounds do
      let a = Dynarray_boxed.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Dynarray_boxed.add_last a v;
      done;
    done

  let[@inline never] add_dynarray_unboxed conf =
    for r = 1 to conf.rounds do
      let a = Dynarray_unboxed.create () in
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Dynarray_unboxed.add_last a v;
      done;
    done
end


module Bench_add_pop = struct
  (* In this benchmark, we share the container between rounds. This is
     representative of workloads where a given container is used
     intensively as a stack: we see the constant factors inherent to
     'add' and 'pop', not the cost of the resizing strategy. *)

  let[@inline never] add_pop_stack conf =
    let a = Stack.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Stack.push v a;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (Stack.pop a));
      done;
    done

  let[@inline never] add_pop_base_stack conf =
    let a = Base_stack.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Base_stack.push a v;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (Base_stack.pop_exn a));
      done;
    done

  let[@inline never] add_pop_queue conf =
    let a = Queue.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Queue.push v a;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (Queue.pop a));
      done;
    done

  let[@inline never] add_pop_ccvector conf =
    let a = CCVector.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        CCVector.push a v;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (CCVector.pop_exn a));
      done;
    done

  let[@inline never] add_pop_dynarray_boxed conf =
    let a = Dynarray_boxed.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Dynarray_boxed.add_last a v;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (Dynarray_boxed.pop_last a));
      done;
    done

  let[@inline never] add_pop_dynarray_unboxed conf =
    let a = Dynarray_unboxed.create () in
    for r = 1 to conf.rounds do
      let v = opaque (Some r) in
      for _i = 0 to conf.length - 1 do
        Dynarray_unboxed.add_last a v;
      done;
      for _i = 0 to conf.length - 1 do
        ignore (opaque (Dynarray_unboxed.pop_last a));
      done;
    done
end


module Bench_iter = struct
  let nothing x = ignore (opaque x)

  let[@inline never] iter_stack conf =
    let data = Array.make conf.length (opaque None) in
    let a = Stack.of_seq @@ Array.to_seq data in
    for _ = 1 to conf.rounds do
      Stack.iter nothing a;
    done

  let[@inline never] iter_base_stack conf =
    let data = Array.make conf.length (opaque None) in
    let a = Base_stack.of_list @@ Array.to_list data in
    for _ = 1 to conf.rounds do
      Base_stack.iter ~f:nothing a;
    done

  let[@inline never] iter_queue conf =
    let data = Array.make conf.length (opaque None) in
    let a = Queue.of_seq @@ Array.to_seq data in
    for _ = 1 to conf.rounds do
      Queue.iter nothing a;
    done

  let[@inline never] iter_ccvector conf =
    let a = CCVector.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      CCVector.iter nothing a;
    done

  let[@inline never] iter_dynarray_boxed conf =
    let module A = Dynarray_boxed in
    let a = A.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      A.iter nothing a;
    done

  let[@inline never] iter_dynarray_unboxed conf =
    let module A = Dynarray_unboxed in
    let a = A.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      A.iter nothing a;
    done
end


module Bench_to_array = struct
  let[@inline never] to_array_stack conf =
    let data = Array.make conf.length (opaque None) in
    let a = Stack.of_seq @@ Array.to_seq data in
    for _ = 1 to conf.rounds do
      let len = Stack.length a in
      let res = Array.make len (Stack.top a) in
      ignore (Stack.fold (fun i v ->
        Array.unsafe_set res i v;
        i-1
      ) (len - 1) a)
    done

  (* TODO: to_array_base_stack *)

  let[@inline never] to_array_queue conf =
    let data = Array.make conf.length (opaque None) in
    let a = Queue.of_seq @@ Array.to_seq data in
    for _ = 1 to conf.rounds do
      let res = Array.make (Queue.length a) (Queue.top a) in
      ignore (Queue.fold (fun i v ->
        Array.unsafe_set res i v;
        i+1
      ) 0 a)
    done

  let[@inline never] to_array_ccvector conf =
    let a = CCVector.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (CCVector.to_array a);
    done

  let[@inline never] to_array_dynarray_boxed conf =
    let module A = Dynarray_boxed in
    let a = A.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.to_array a);
    done

  let[@inline never] to_array_dynarray_unboxed conf =
    let module A = Dynarray_unboxed in
    let a = A.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.to_array a);
    done
end

module Bench_of_array = struct
  let[@inline never] of_array_stack conf =
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      let a = Stack.create () in
      for i = 0 to conf.length - 1 do
        Stack.push (Array.unsafe_get data i) a;
      done;
    done

  (* TODO: of_array_base_stack *)

  let[@inline never] of_array_queue conf =
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      let a = Queue.create () in
      for i = 0 to conf.length - 1 do
        Queue.push (Array.unsafe_get data i) a;
      done;
    done

  let[@inline never] of_array_ccvector conf =
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (CCVector.of_array data);
    done

  let[@inline never] of_array_dynarray_boxed conf =
    let module A = Dynarray_boxed in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.of_array data);
    done

  let[@inline never] of_array_dynarray_unboxed conf =
    let module A = Dynarray_unboxed in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.of_array data);
    done
end

module Bench_of_list = struct
  let[@inline never] of_list_stack conf =
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      let a = Stack.create () in
      List.iter (fun v -> Stack.push v a) data
    done

  let[@inline never] of_list_base_stack conf =
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      ignore (Base_stack.of_list data);
    done

  let[@inline never] of_list_queue conf =
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      let a = Queue.create () in
      List.iter (fun v -> Queue.push v a) data
    done

  let[@inline never] of_list_ccvector conf =
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      ignore (CCVector.of_list data);
    done

  let[@inline never] of_list_dynarray_boxed conf =
    let module A = Dynarray_boxed in
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      ignore (A.of_list data);
    done

  let[@inline never] of_list_dynarray_unboxed conf =
    let module A = Dynarray_unboxed in
    let v = opaque None in
    let data = List.init conf.length (fun _ -> v) in
    for _ = 1 to conf.rounds do
      ignore (A.of_list data);
    done
end

module Bench_append_array = struct
  (* In this benchmark we intentionally share the structure between
     each round and 'clear' it (without deallocating the
     backing array) at each round, to avoid measuring the cost of
     resizing on growth. *)

  let[@inline never] append_array_stack conf =
    let a = Stack.create () in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      for i = 0 to conf.length - 1 do
        Stack.push (Array.unsafe_get data i) a;
      done;
      Stack.clear a;
    done

  (* TODO: append_array_base_stack *)

  let[@inline never] append_array_queue conf =
    let a = Queue.create () in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      for i = 0 to conf.length - 1 do
        Queue.push (Array.unsafe_get data i) a;
      done;
      Queue.clear a;
    done

  let[@inline never] append_array_ccvector conf =
    let a = CCVector.create () in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (CCVector.append_array a data);
      CCVector.clear a;
    done

  let[@inline never] append_array_dynarray_boxed conf =
    let module A = Dynarray_boxed in
    let a = A.create () in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.append_array a data);
      A.clear a;
    done

  let[@inline never] append_array_dynarray_unboxed conf =
    let module A = Dynarray_unboxed in
    let a = A.create () in
    let data = Array.make conf.length (opaque None) in
    for _ = 1 to conf.rounds do
      ignore (A.append_array a data);
      A.clear a;
    done
end

(** {2 Entry point} *)

let () =
  match bench with
  | Get ->
    let module B = Bench_get in
    begin match impl with
      | Array -> B.get_array conf
      | Base_stack -> B.get_base_stack conf
      | CCVector -> B.get_ccvector conf
      | Dynarray_boxed -> B.get_dynarray_boxed conf
      | Dynarray_unboxed -> B.get_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Set ->
    let module B = Bench_set in
    begin match impl with
      | Array -> B.set_array conf
      | Base_stack -> B.set_base_stack conf
      | CCVector -> B.set_ccvector conf
      | Dynarray_boxed -> B.set_dynarray_boxed conf
      | Dynarray_unboxed -> B.set_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Add ->
    let module B = Bench_add in
    begin match impl with
      | Stack -> B.add_stack conf
      | Base_stack -> B.add_base_stack conf
      | Queue -> B.add_queue conf
      | CCVector -> B.add_ccvector conf
      | Dynarray_boxed -> B.add_dynarray_boxed conf
      | Dynarray_unboxed -> B.add_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Add_pop ->
    let module B = Bench_add_pop in
    begin match impl with
      | Stack -> B.add_pop_stack conf
      | Base_stack -> B.add_pop_base_stack conf
      | Queue -> B.add_pop_queue conf
      | CCVector -> B.add_pop_ccvector conf
      | Dynarray_boxed -> B.add_pop_dynarray_boxed conf
      | Dynarray_unboxed -> B.add_pop_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Iter ->
    let module B = Bench_iter in
    begin match impl with
      | Stack -> B.iter_stack conf
      | Base_stack -> B.iter_base_stack conf
      | Queue -> B.iter_queue conf
      | CCVector -> B.iter_ccvector conf
      | Dynarray_boxed -> B.iter_dynarray_boxed conf
      | Dynarray_unboxed -> B.iter_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | To_array ->
    let module B = Bench_to_array in
    begin match impl with
      | Stack -> B.to_array_stack conf
      | Base_stack -> failwith "TODO"
      | Queue -> B.to_array_queue conf
      | CCVector -> B.to_array_ccvector conf
      | Dynarray_boxed -> B.to_array_dynarray_boxed conf
      | Dynarray_unboxed -> B.to_array_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Of_array ->
    let module B = Bench_of_array in
    begin match impl with
      | Stack -> B.of_array_stack conf
      | Base_stack -> failwith "TODO"
      | Queue -> B.of_array_queue conf
      | CCVector -> B.of_array_ccvector conf
      | Dynarray_boxed -> B.of_array_dynarray_boxed conf
      | Dynarray_unboxed -> B.of_array_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Of_list ->
    let module B = Bench_of_list in
    begin match impl with
      | Stack -> B.of_list_stack conf
      | Base_stack -> B.of_list_base_stack conf
      | Queue -> B.of_list_queue conf
      | CCVector -> B.of_list_ccvector conf
      | Dynarray_boxed -> B.of_list_dynarray_boxed conf
      | Dynarray_unboxed -> B.of_list_dynarray_unboxed conf
      | _ -> unsupported ()
    end

  | Append_array ->
    let module B = Bench_append_array in
    begin match impl with
      | Stack -> B.append_array_stack conf
      | Base_stack -> failwith "TODO"
      | Queue -> B.append_array_queue conf
      | CCVector -> B.append_array_ccvector conf
      | Dynarray_boxed -> B.append_array_dynarray_boxed conf
      | Dynarray_unboxed -> B.append_array_dynarray_unboxed conf
      | _ -> unsupported ()
    end
