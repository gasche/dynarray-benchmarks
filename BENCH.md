# Dynarray benchmarks

## Introduction

### Implementations

- `Array`: the stdlib Array module, which is only used to compare the
  performance of random access operations (`get` and `set`).

- `Stack`: the stdlib Stack module, which uses a mutable reference to
  a list. This is supposed to be slower due to worse locality than
  arrays, but in practice it is faster for most workloads due to not
  needing resizing which causes a constant factor of reallocations.

- `Queue`: the stdlib `Queue` module, which uses a mutable linked
  list. This should be slightly slower than `Stack` are there are more
  mutations involved. (Note: the `pop` benchmarks will pop different
  elements with the `Stack` and `Queue` implementations, they are not
  expected to be equivalent programs.)

- `Dynarray_boxed`: the Dynarray implementation that got merged in the
  stdlib in OCaml 5.2, which uses a "boxed" representation: an `'a
  slot array`, with `type slot = Empty | Elem of { mutable v : 'a }`.
  We expect this to be slower than unboxed representations (using `'a
  array` directly), especially for `add`-heavy workflows which need to
  allocate a new `Elem` slot on each addition, or on very large input
  sizes where the lack of data locality hurts.

- `Dynarray_unboxed`: the unboxed implementation of Dynarray proposed
  in [#12885](https://github.com/ocaml/ocaml/pull/12885). It uses
  a "local dummy", where different backing arrays may use distinct
  dummies for empty elements, and ensure that the backing arrays are
  never flat float arrays.

- `CCVector`: dynarrays from the
  [Containers](https://github.com/c-cube/ocaml-containers/)
  library. They are unboxed and erase elements on pop, so the
  implementation is very close to `Dynarray_unboxed`. It uses
  a "global" dummy, which is slightly faster (but fully safe
  on Multicore), but performs a dynamic check to support flat float
  dynarrays.

- `Base_stack`: the `Stack` module of the
   [Base](https://github.com/janestreet/base/) library, which is
   implemented as a dynamic array. It uses yet another flavor of
   dummies, and statically ensures that backing arrays are never flat
   float arrays. We added `get` and `set` functions (not exposed
   in Base) to also support random-access benchmarks.

### Input sizes

By default my microbenchmarks use "medium"-size lists of
size 1000. I also measured performance on "small" lists of size 10 and
"large" lists of size 1_000_000. I only report on those results when
they differ substantially.

### Flambda vs. non-flambda

I ran all benchmarks with both flambda and non-flambda compilers. The
results are almost-identical most of the time, and sometimes 10%-20%
better with flambda, or even larger gains for Base_stack which seems
written with flambda in mind. I thus report the flambda results.

### Caveats

These microbenchmarks are designed to measure only the cost of
datastructure access, which rarely dominate in real-world programs
where data access comes with computations on the data. In this
context, one should not be shocked by large performance ratios. An
operation running 2x slower than another is a *small* difference --
for example, using `Hashtbl` instead of `Array` is 42x slower on some
benchmarks, despite being perfectly acceptable for most workloads.

In our experience, the relative performance comparisons between the
various implementations are very sensitive to details of the
benchmarking setup. For example, of first round of results suggested
that CCVector is around 20% faster than most other dynarray
implementations, and then we ensured that we use the Dune `--release`
profile for our benchmarks and that performance advantage disappeared.

The main advantage of unboxed representations is to avoid surprises
regarding memory layout and usage, and to benefit from better
locality. Locality effects are hard to measure, especially in
microbenchmarks, so let us keep in mind that there are probably more
performance benefits in real-world programs than shown in the
micro-benchmark results.

## Results

### get

Random-access benchmark.

`Array` is *much* faster than all other implementations if you just
measure random access: the OCaml compiler generates excellent code for
the bound check, and then a direct access. Any extra layer of logic
makes the code sensibly slower.

| Command                   |     Mean [ms] |    Relative |
|:--------------------------|--------------:|------------:|
| `Array (1000)`            |  886.3 ± 18.7 |        1.00 |
| `CCVector (1000)`         | 1639.6 ± 47.9 | 1.85 ± 0.07 |
| `Dynarray_boxed (1000)`   | 1924.8 ± 47.7 | 2.17 ± 0.07 |
| `Dynarray_unboxed (1000)` | 1947.9 ± 32.5 | 2.20 ± 0.06 |
| `Base_stack (1000)`       | 3724.1 ± 37.7 | 4.20 ± 0.10 |

`CCVector` does better than the `Dynarray` implementations in these
results because its definition of `get` force inlining, while the
other implementations do not. Otherwise the performance are pretty
close, with `Base_stack` lagging behind due to having more function
calls. (It may be that the standard Jane Street build flags use more
agressive inlining settings.)

`Dynarray_unboxed` has one less indirection to follow on `get`, which
should increase performance slightly, but it then check whether the
value it read was the dummy, which makes things slightly slower
(the check never fails in practice so is predicted perfectly). It
looks like the two effects balance each other, so the boxed and
unboxed implementations are within measurement noise.

### set

Random-access benchmark.
The results for `set` are extremely similar to the results of `get`.

| Command                   |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `Array (1000)`            |  201.7 ± 3.8 |        1.00 |
| `CCVector (1000)`         |  247.3 ± 8.2 | 1.23 ± 0.05 |
| `Dynarray_boxed (1000)`   |  276.9 ± 5.8 | 1.37 ± 0.04 |
| `Dynarray_unboxed (1000)` | 280.1 ± 24.7 | 1.39 ± 0.13 |
| `Base_stack (1000)`       | 412.2 ± 15.7 | 2.04 ± 0.09 |

### add

This benchmark measures the performance of adding a lot of elements,
starting from an empty dynarray. This measures the cost of resizing
the array as the number of element grows.

| Command                   |     Mean [ms] |    Relative |
|:--------------------------|--------------:|------------:|
| `Stack (1000)`            |   173.6 ± 4.4 |        1.00 |
| `Queue (1000)`            |   244.1 ± 3.9 | 1.41 ± 0.04 |
| `Dynarray_unboxed (1000)` |  579.0 ± 24.8 | 3.33 ± 0.17 |
| `Base_stack (1000)`       |  598.6 ± 18.2 | 3.45 ± 0.14 |
| `CCVector (1000)`         | 679.9 ± 215.2 | 3.92 ± 1.24 |
| `Dynarray_boxed (1000)`   |  949.8 ± 22.9 | 5.47 ± 0.19 |

The linked-list based `Stack` and `Queue` are faster (for small to
middle-sized lists) than the array-based implementations, as they do
not need to repeatedly resize the structure. Among array-based
implementations, the `Dynarray_boxed` implementation is sensibly
slower than the others, because it needs to allocate a new slot on
each new value.

On large lists, the unboxed array-based implementations are faster
than the other, probably due to locality benefits.

| Command                      |      Mean [s] |    Relative |
|:-----------------------------|--------------:|------------:|
| `Base_stack (1000000)`       | 0.601 ± 0.010 |        1.00 |
| `Dynarray_unboxed (1000000)` | 0.873 ± 0.021 | 1.45 ± 0.04 |
| `CCVector (1000000)`         | 0.903 ± 0.015 | 1.50 ± 0.04 |
| `Stack (1000000)`            | 1.007 ± 0.042 | 1.68 ± 0.07 |
| `Queue (1000000)`            | 1.097 ± 0.030 | 1.83 ± 0.06 |
| `Dynarray_boxed (1000000)`   | 1.547 ± 0.054 | 2.57 ± 0.10 |

I am not sure where the performance advantage of `Base_stack` comes
from. It may come from the unsafe tricks it uses to tell the compiler
that its backing array cannot contain floating-point numbers, so that
the dynamic check for flat float arrays is optimized away.

### add_pop

This benchmark measures the cost of repeatedly adding and removing
elements in a steady state; the cost of resizing the array should be
amortized and not show up in the results.

The results are similar to `add`, with surprisingly good results for
`Dynarray_boxed`:

| Command                   |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `Stack (1000)`            | 327.3 ± 12.8 |        1.00 |
| `Queue (1000)`            | 426.7 ± 6.7  | 1.30 ± 0.06 |
| `Dynarray_boxed (1000)`   | 494.4 ± 15.3 | 1.51 ± 0.08 |
| `Dynarray_unboxed (1000)` | 558.4 ± 15.6 | 1.71 ± 0.08 |
| `Base_stack (1000)`       | 664.0 ± 18.6 | 2.03 ± 0.10 |
| `CCVector (1000)`         | 710.2 ± 22.4 | 2.17 ± 0.11 |


Array-based structures become substantially faster on large inputs,
especially the unboxed ones:

| Command                      |      Mean [s] |    Relative |
|:-----------------------------|--------------:|------------:|
| `Dynarray_unboxed (1000000)` | 0.527 ± 0.017 |        1.00 |
| `Base_stack (1000000)`       | 0.621 ± 0.015 | 1.18 ± 0.05 |
| `CCVector (1000000)`         | 0.650 ± 0.017 | 1.23 ± 0.05 |
| `Dynarray_boxed (1000000)`   | 1.517 ± 0.036 | 2.88 ± 0.12 |
| `Queue (1000000)`            | 1.941 ± 0.035 | 3.69 ± 0.14 |
| `Stack (1000000)`            | 2.069 ± 0.051 | 3.93 ± 0.16 |

### iter

The performance of the `iter` function appears to be very close with
each structure. `Base_stack` seems slower than the other, possibly
because its `iter` implementation does not use unsafe array accesses:

| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Dynarray_boxed (1000)` | 66.7 ± 3.7 | 62.2 | 78.3 | 1.00 |
| `Queue (1000)` | 72.3 ± 1.8 | 70.1 | 79.0 | 1.08 ± 0.07 |
| `CCVector (1000)` | 76.3 ± 3.0 | 72.6 | 86.6 | 1.14 ± 0.08 |
| `Stack (1000)` | 79.0 ± 12.3 | 71.5 | 133.1 | 1.18 ± 0.20 |
| `Dynarray_unboxed (1000)` | 83.2 ± 3.8 | 78.2 | 94.8 | 1.25 ± 0.09 |
| `Base_stack (1000)` | 123.0 ± 3.9 | 114.4 | 136.4 | 1.85 ± 0.12 |

### of_list

The performance of most `of_list` functions are very close, except for
`Dynarray_boxed` which is sensibly slower than the other.

| Command                   |     Mean [ms] |    Relative |
|:--------------------------|--------------:|------------:|
| `Stack (1000)`            |  219.4 ± 20.4 |        1.00 |
| `Dynarray_unboxed (1000)` |   251.9 ± 7.5 | 1.15 ± 0.11 |
| `Queue (1000)`            |   284.0 ± 6.8 | 1.29 ± 0.12 |
| `Base_stack (1000)`       |   287.8 ± 6.7 | 1.31 ± 0.13 |
| `CCVector (1000)`         |  318.4 ± 10.6 | 1.45 ± 0.14 |
| `Dynarray_boxed (1000)`   | 1015.3 ± 23.5 | 4.63 ± 0.44 |

The cause for the slowdown of `Dynarray_boxed` is that it does not
precompute the size of the structure to avoid extra resizing. We
contributed this optimization upstream in
[#12883](https://github.com/ocaml/ocaml/pull/12883) but the version in
the benchmark predates that PR.

The implementation of `Dynarray_unboxed` uses `Array.of_list` to
create an array of all elements at once, while other dynarray
implementations repeatedly call their `add` function, which explains
that they are somewhat slower.

### of_array

`of_array` is very simple for `CCVector` and `Dynarray_unboxed`, they
just copy the input array. `Dynarray_boxed` is sensibly slower than
all other; we suspect that this is due to the higher GC pressure of
the unboxed representation.

| Command                   |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `CCVector (1000)`         |  129.5 ± 3.5 |        1.00 |
| `Dynarray_unboxed (1000)` |  140.9 ± 8.8 | 1.09 ± 0.07 |
| `Stack (1000)`            | 198.2 ± 12.7 | 1.53 ± 0.11 |
| `Queue (1000)`            |  254.3 ± 4.1 | 1.96 ± 0.06 |
| `Dynarray_boxed (1000)`   | 861.2 ± 17.8 | 6.65 ± 0.22 |


### to_array

`to_array` remains very simple for `CCVector`, which assumes that the
"user space" of the array may not contain dummies and directly copies
the array -- this assumption is incorrect for OCaml 5. All other
implementations must iterate on each value in turn and perform very
close to each other.

| Command                   |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `CCVector (1000)`         |  128.6 ± 3.6 |        1.00 |
| `Dynarray_boxed (1000)`   |  225.1 ± 5.6 | 1.75 ± 0.07 |
| `Stack (1000)`            |  238.3 ± 5.4 | 1.85 ± 0.07 |
| `Dynarray_unboxed (1000)` | 243.9 ± 11.4 | 1.90 ± 0.10 |
| `Queue (1000)`            |  249.4 ± 6.8 | 1.94 ± 0.08 |
