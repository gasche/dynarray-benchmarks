| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (10)` | 87.0 ± 2.8 | 82.0 | 93.1 | 1.00 |
| `Dynarray_boxed (10)` | 234.5 ± 5.6 | 223.3 | 257.6 | 2.69 ± 0.11 |
| `Dynarray_unboxed (10)` | 244.0 ± 5.0 | 236.8 | 259.3 | 2.80 ± 0.11 |
| `Stack (10)` | 244.1 ± 6.9 | 232.0 | 263.1 | 2.80 ± 0.12 |
| `Queue (10)` | 251.3 ± 17.7 | 237.5 | 364.5 | 2.89 ± 0.22 |

<!--
IMPL={impl} BENCH=To_array ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
