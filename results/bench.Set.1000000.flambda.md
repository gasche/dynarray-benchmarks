| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (1000000)` | 341.2 ± 8.6 | 330.6 | 374.9 | 1.00 |
| `Dynarray_unboxed (1000000)` | 366.0 ± 10.2 | 354.2 | 384.3 | 1.07 ± 0.04 |
| `CCVector (1000000)` | 374.0 ± 13.9 | 355.9 | 424.9 | 1.10 ± 0.05 |
| `Dynarray_boxed (1000000)` | 413.6 ± 7.6 | 407.7 | 456.4 | 1.21 ± 0.04 |
| `Base_stack (1000000)` | 648.9 ± 242.9 | 555.1 | 2322.8 | 1.90 ± 0.71 |

<!--
IMPL={impl} BENCH=Set ROUNDS=40 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
