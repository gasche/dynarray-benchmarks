| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000000)` | 83.5 ± 1.9 | 80.8 | 88.6 | 1.00 |
| `Dynarray_unboxed (1000000)` | 87.2 ± 1.3 | 85.3 | 91.7 | 1.04 ± 0.03 |
| `Dynarray_boxed (1000000)` | 132.1 ± 2.0 | 129.7 | 141.4 | 1.58 ± 0.04 |
| `Stack (1000000)` | 140.9 ± 1.6 | 137.6 | 145.3 | 1.69 ± 0.04 |
| `Queue (1000000)` | 144.3 ± 3.5 | 140.2 | 158.1 | 1.73 ± 0.06 |
| `Base_stack (1000000)` | 207.1 ± 3.2 | 203.7 | 218.0 | 2.48 ± 0.07 |

<!--
IMPL={impl} BENCH=Iter ROUNDS=10 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
