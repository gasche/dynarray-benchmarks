| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000000)` | 211.0 ± 3.3 | 205.9 | 220.8 | 1.00 |
| `Dynarray_unboxed (1000000)` | 212.2 ± 4.1 | 207.3 | 220.8 | 1.01 ± 0.03 |
| `Stack (1000000)` | 925.9 ± 15.9 | 913.7 | 1000.2 | 4.39 ± 0.10 |
| `Queue (1000000)` | 1006.3 ± 23.7 | 987.5 | 1155.4 | 4.77 ± 0.13 |
| `Dynarray_boxed (1000000)` | 1084.8 ± 51.2 | 1062.0 | 1417.1 | 5.14 ± 0.26 |

<!--
IMPL={impl} BENCH=Of_array ROUNDS=20 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
