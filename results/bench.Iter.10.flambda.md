| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Queue (10)` | 68.3 ± 2.2 | 65.3 | 74.4 | 1.00 |
| `Dynarray_boxed (10)` | 68.6 ± 0.6 | 67.8 | 70.7 | 1.01 ± 0.03 |
| `Stack (10)` | 73.4 ± 2.8 | 69.5 | 79.7 | 1.08 ± 0.05 |
| `CCVector (10)` | 77.8 ± 2.0 | 75.0 | 83.1 | 1.14 ± 0.05 |
| `Dynarray_unboxed (10)` | 87.5 ± 0.8 | 86.0 | 89.8 | 1.28 ± 0.04 |
| `Base_stack (10)` | 123.7 ± 1.9 | 121.9 | 131.8 | 1.81 ± 0.06 |

<!--
IMPL={impl} BENCH=Iter ROUNDS=1000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
