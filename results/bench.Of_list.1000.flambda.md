| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (1000)` | 219.4 ± 20.4 | 206.4 | 352.8 | 1.00 |
| `Dynarray_unboxed (1000)` | 251.9 ± 7.5 | 237.7 | 267.7 | 1.15 ± 0.11 |
| `Queue (1000)` | 284.0 ± 6.8 | 269.3 | 298.4 | 1.29 ± 0.12 |
| `Base_stack (1000)` | 287.8 ± 6.7 | 276.7 | 300.3 | 1.31 ± 0.13 |
| `CCVector (1000)` | 318.4 ± 10.6 | 302.7 | 342.2 | 1.45 ± 0.14 |
| `Dynarray_boxed (1000)` | 1015.3 ± 23.5 | 981.4 | 1116.3 | 4.63 ± 0.44 |

<!--
IMPL={impl} BENCH=Of_list ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
