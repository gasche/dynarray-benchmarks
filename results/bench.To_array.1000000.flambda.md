| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000000)` | 215.0 ± 12.1 | 198.6 | 250.6 | 1.00 |
| `Dynarray_unboxed (1000000)` | 317.4 ± 7.1 | 307.3 | 343.0 | 1.48 ± 0.09 |
| `Queue (1000000)` | 434.7 ± 17.3 | 424.7 | 550.0 | 2.02 ± 0.14 |
| `Stack (1000000)` | 439.5 ± 17.9 | 412.1 | 483.6 | 2.04 ± 0.14 |
| `Dynarray_boxed (1000000)` | 452.8 ± 18.7 | 431.4 | 516.4 | 2.11 ± 0.15 |

<!--
IMPL={impl} BENCH=To_array ROUNDS=20 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
