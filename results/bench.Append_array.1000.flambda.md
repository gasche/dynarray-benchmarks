| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000)` | 209.5 ± 6.7 | 198.4 | 222.9 | 1.00 |
| `Stack (1000)` | 218.9 ± 9.5 | 198.2 | 235.9 | 1.04 ± 0.06 |
| `Dynarray_unboxed (1000)` | 228.3 ± 7.6 | 208.7 | 241.8 | 1.09 ± 0.05 |
| `Queue (1000)` | 295.9 ± 10.7 | 275.5 | 317.1 | 1.41 ± 0.07 |
| `Dynarray_boxed (1000)` | 314.6 ± 7.7 | 306.4 | 340.7 | 1.50 ± 0.06 |

<!--
IMPL={impl} BENCH=Append_array ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
