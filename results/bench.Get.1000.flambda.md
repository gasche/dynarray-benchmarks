| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (1000)` | 886.3 ± 18.7 | 864.9 | 941.1 | 1.00 |
| `CCVector (1000)` | 1639.6 ± 47.9 | 1598.2 | 1772.7 | 1.85 ± 0.07 |
| `Dynarray_boxed (1000)` | 1924.8 ± 47.7 | 1890.6 | 2232.9 | 2.17 ± 0.07 |
| `Dynarray_unboxed (1000)` | 1947.9 ± 32.5 | 1912.1 | 2092.9 | 2.20 ± 0.06 |
| `Base_stack (1000)` | 3724.1 ± 37.7 | 3689.4 | 3905.2 | 4.20 ± 0.10 |

<!--
IMPL={impl} BENCH=Get ROUNDS=100000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
