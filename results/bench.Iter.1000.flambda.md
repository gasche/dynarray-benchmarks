| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Dynarray_boxed (1000)` | 66.7 ± 3.7 | 62.2 | 78.3 | 1.00 |
| `Queue (1000)` | 72.3 ± 1.8 | 70.1 | 79.0 | 1.08 ± 0.07 |
| `CCVector (1000)` | 76.3 ± 3.0 | 72.6 | 86.6 | 1.14 ± 0.08 |
| `Stack (1000)` | 79.0 ± 12.3 | 71.5 | 133.1 | 1.18 ± 0.20 |
| `Dynarray_unboxed (1000)` | 83.2 ± 3.8 | 78.2 | 94.8 | 1.25 ± 0.09 |
| `Base_stack (1000)` | 123.0 ± 3.9 | 114.4 | 136.4 | 1.85 ± 0.12 |

<!--
IMPL={impl} BENCH=Iter ROUNDS=10000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
