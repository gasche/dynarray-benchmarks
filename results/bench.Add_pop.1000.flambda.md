| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (1000)` | 327.3 ± 12.8 | 308.7 | 372.7 | 1.00 |
| `Queue (1000)` | 426.7 ± 6.7 | 414.5 | 440.8 | 1.30 ± 0.06 |
| `Dynarray_boxed (1000)` | 494.4 ± 15.3 | 475.3 | 569.0 | 1.51 ± 0.08 |
| `Dynarray_unboxed (1000)` | 558.4 ± 15.6 | 536.6 | 600.3 | 1.71 ± 0.08 |
| `Base_stack (1000)` | 664.0 ± 18.6 | 633.4 | 714.8 | 2.03 ± 0.10 |
| `CCVector (1000)` | 710.2 ± 22.4 | 670.9 | 779.8 | 2.17 ± 0.11 |

<!--
IMPL={impl} BENCH=Add_pop ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
