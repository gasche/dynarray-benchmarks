| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (10)` | 186.5 ± 2.9 | 177.3 | 191.0 | 1.00 |
| `Queue (10)` | 239.4 ± 4.3 | 225.7 | 259.7 | 1.28 ± 0.03 |
| `Dynarray_boxed (10)` | 519.4 ± 7.2 | 508.8 | 539.2 | 2.79 ± 0.06 |
| `Base_stack (10)` | 528.5 ± 11.5 | 511.6 | 563.7 | 2.83 ± 0.08 |
| `Dynarray_unboxed (10)` | 545.6 ± 148.9 | 513.7 | 1575.4 | 2.93 ± 0.80 |
| `CCVector (10)` | 570.7 ± 11.9 | 557.5 | 625.9 | 3.06 ± 0.08 |

<!--
IMPL={impl} BENCH=Add ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
