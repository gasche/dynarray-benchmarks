| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Dynarray_unboxed (10)` | 130.9 ± 12.1 | 117.5 | 199.6 | 1.00 |
| `CCVector (10)` | 237.5 ± 6.3 | 219.8 | 248.8 | 1.81 ± 0.17 |
| `Stack (10)` | 239.3 ± 15.5 | 224.4 | 334.6 | 1.83 ± 0.21 |
| `Dynarray_boxed (10)` | 331.5 ± 7.3 | 317.8 | 347.2 | 2.53 ± 0.24 |
| `Queue (10)` | 335.8 ± 23.8 | 322.3 | 453.1 | 2.56 ± 0.30 |

<!--
IMPL={impl} BENCH=Append_array ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
