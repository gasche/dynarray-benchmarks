| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (10)` | 984.4 ± 76.0 | 914.5 | 1164.2 | 1.00 |
| `CCVector (10)` | 1832.1 ± 42.3 | 1770.2 | 1979.9 | 1.86 ± 0.15 |
| `Dynarray_boxed (10)` | 2242.5 ± 32.4 | 2205.1 | 2336.2 | 2.28 ± 0.18 |
| `Dynarray_unboxed (10)` | 2331.8 ± 62.5 | 2282.9 | 2606.7 | 2.37 ± 0.19 |
| `Base_stack (10)` | 3927.7 ± 57.3 | 3862.2 | 4098.0 | 3.99 ± 0.31 |

<!--
IMPL={impl} BENCH=Get ROUNDS=10000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
