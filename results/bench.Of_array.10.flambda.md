| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (10)` | 89.4 ± 2.0 | 85.6 | 96.3 | 1.00 |
| `Dynarray_unboxed (10)` | 102.6 ± 5.0 | 94.5 | 116.6 | 1.15 ± 0.06 |
| `Stack (10)` | 208.8 ± 9.9 | 183.9 | 238.0 | 2.33 ± 0.12 |
| `Dynarray_boxed (10)` | 246.1 ± 14.9 | 225.0 | 287.3 | 2.75 ± 0.18 |
| `Queue (10)` | 258.0 ± 6.0 | 249.2 | 270.9 | 2.88 ± 0.09 |

<!--
IMPL={impl} BENCH=Of_array ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
