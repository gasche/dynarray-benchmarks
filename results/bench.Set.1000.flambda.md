| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (1000)` | 201.7 ± 3.8 | 197.1 | 216.4 | 1.00 |
| `CCVector (1000)` | 247.3 ± 8.2 | 238.1 | 268.1 | 1.23 ± 0.05 |
| `Dynarray_boxed (1000)` | 276.9 ± 5.8 | 256.8 | 290.0 | 1.37 ± 0.04 |
| `Dynarray_unboxed (1000)` | 280.1 ± 24.7 | 260.9 | 413.5 | 1.39 ± 0.13 |
| `Base_stack (1000)` | 412.2 ± 15.7 | 372.7 | 446.2 | 2.04 ± 0.09 |

<!--
IMPL={impl} BENCH=Set ROUNDS=40000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
