| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Dynarray_unboxed (1000000)` | 241.4 ± 6.8 | 230.2 | 255.1 | 1.00 |
| `CCVector (1000000)` | 301.0 ± 4.0 | 294.6 | 312.8 | 1.25 ± 0.04 |
| `Stack (1000000)` | 895.8 ± 13.1 | 878.2 | 939.1 | 3.71 ± 0.12 |
| `Dynarray_boxed (1000000)` | 975.2 ± 23.7 | 959.1 | 1122.5 | 4.04 ± 0.15 |
| `Queue (1000000)` | 1003.9 ± 11.4 | 987.6 | 1049.5 | 4.16 ± 0.13 |

<!--
IMPL={impl} BENCH=Append_array ROUNDS=20 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
