| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (10)` | 238.7 ± 23.0 | 228.9 | 395.6 | 1.00 |
| `CCVector (10)` | 258.3 ± 5.9 | 249.6 | 279.6 | 1.08 ± 0.11 |
| `Dynarray_unboxed (10)` | 280.7 ± 20.9 | 271.2 | 378.9 | 1.18 ± 0.14 |
| `Dynarray_boxed (10)` | 286.9 ± 36.3 | 273.0 | 506.8 | 1.20 ± 0.19 |
| `Base_stack (10)` | 457.3 ± 283.3 | 398.4 | 2420.0 | 1.92 ± 1.20 |

<!--
IMPL={impl} BENCH=Set ROUNDS=4000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
