| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Array (1000000)` | 910.1 ± 230.3 | 781.3 | 2473.3 | 1.00 |
| `CCVector (1000000)` | 1636.6 ± 43.7 | 1593.2 | 1789.9 | 1.80 ± 0.46 |
| `Dynarray_unboxed (1000000)` | 1967.5 ± 36.6 | 1920.7 | 2118.3 | 2.16 ± 0.55 |
| `Dynarray_boxed (1000000)` | 2109.7 ± 77.4 | 2045.6 | 2380.0 | 2.32 ± 0.59 |
| `Base_stack (1000000)` | 3785.2 ± 47.6 | 3738.2 | 3966.6 | 4.16 ± 1.05 |

<!--
IMPL={impl} BENCH=Get ROUNDS=100 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
