| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000)` | 128.6 ± 3.6 | 123.9 | 138.3 | 1.00 |
| `Dynarray_boxed (1000)` | 225.1 ± 5.6 | 218.4 | 245.3 | 1.75 ± 0.07 |
| `Stack (1000)` | 238.3 ± 5.4 | 227.4 | 247.8 | 1.85 ± 0.07 |
| `Dynarray_unboxed (1000)` | 243.9 ± 11.4 | 221.8 | 283.6 | 1.90 ± 0.10 |
| `Queue (1000)` | 249.4 ± 6.8 | 235.0 | 269.6 | 1.94 ± 0.08 |

<!--
IMPL={impl} BENCH=To_array ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
