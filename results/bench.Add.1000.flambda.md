| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (1000)` | 173.6 ± 4.4 | 168.4 | 181.1 | 1.00 |
| `Queue (1000)` | 244.1 ± 3.9 | 240.8 | 264.5 | 1.41 ± 0.04 |
| `Dynarray_unboxed (1000)` | 579.0 ± 24.8 | 545.5 | 653.0 | 3.33 ± 0.17 |
| `Base_stack (1000)` | 598.6 ± 18.2 | 571.8 | 646.1 | 3.45 ± 0.14 |
| `CCVector (1000)` | 679.9 ± 215.2 | 593.7 | 2156.8 | 3.92 ± 1.24 |
| `Dynarray_boxed (1000)` | 949.8 ± 22.9 | 919.1 | 1012.8 | 5.47 ± 0.19 |

<!--
IMPL={impl} BENCH=Add ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
