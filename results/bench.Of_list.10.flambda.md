| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (10)` | 218.4 ± 9.2 | 207.8 | 265.3 | 1.00 |
| `Dynarray_unboxed (10)` | 237.4 ± 6.5 | 226.1 | 261.4 | 1.09 ± 0.05 |
| `Base_stack (10)` | 277.5 ± 8.4 | 266.0 | 295.0 | 1.27 ± 0.07 |
| `Queue (10)` | 290.7 ± 8.6 | 280.0 | 312.7 | 1.33 ± 0.07 |
| `CCVector (10)` | 332.2 ± 6.9 | 322.7 | 350.7 | 1.52 ± 0.07 |
| `Dynarray_boxed (10)` | 574.8 ± 9.9 | 555.1 | 609.0 | 2.63 ± 0.12 |

<!--
IMPL={impl} BENCH=Of_list ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
