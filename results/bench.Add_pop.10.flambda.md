| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Stack (10)` | 365.7 ± 15.6 | 338.8 | 408.1 | 1.00 |
| `Queue (10)` | 469.5 ± 40.9 | 431.3 | 676.6 | 1.28 ± 0.12 |
| `Dynarray_boxed (10)` | 503.9 ± 38.3 | 471.5 | 739.0 | 1.38 ± 0.12 |
| `Dynarray_unboxed (10)` | 542.2 ± 13.1 | 517.9 | 588.2 | 1.48 ± 0.07 |
| `Base_stack (10)` | 663.8 ± 71.1 | 629.1 | 1144.3 | 1.81 ± 0.21 |
| `CCVector (10)` | 676.8 ± 15.0 | 653.5 | 736.8 | 1.85 ± 0.09 |

<!--
IMPL={impl} BENCH=Add_pop ROUNDS=2000000 LENGTH=10 dune exec ./bin/bench.flambda.exe
-->
