| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `CCVector (1000)` | 129.5 ± 3.5 | 125.4 | 141.1 | 1.00 |
| `Dynarray_unboxed (1000)` | 140.9 ± 8.8 | 131.8 | 162.6 | 1.09 ± 0.07 |
| `Stack (1000)` | 198.2 ± 12.7 | 179.9 | 236.7 | 1.53 ± 0.11 |
| `Queue (1000)` | 254.3 ± 4.1 | 246.3 | 268.7 | 1.96 ± 0.06 |
| `Dynarray_boxed (1000)` | 861.2 ± 17.8 | 836.9 | 924.3 | 6.65 ± 0.22 |

<!--
IMPL={impl} BENCH=Of_array ROUNDS=20000 LENGTH=1000 dune exec ./bin/bench.flambda.exe
-->
