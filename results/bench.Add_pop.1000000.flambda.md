| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `Dynarray_unboxed (1000000)` | 0.527 ± 0.017 | 0.501 | 0.572 | 1.00 |
| `Base_stack (1000000)` | 0.621 ± 0.015 | 0.592 | 0.659 | 1.18 ± 0.05 |
| `CCVector (1000000)` | 0.650 ± 0.017 | 0.629 | 0.741 | 1.23 ± 0.05 |
| `Dynarray_boxed (1000000)` | 1.517 ± 0.036 | 1.461 | 1.661 | 2.88 ± 0.12 |
| `Queue (1000000)` | 1.941 ± 0.035 | 1.873 | 2.017 | 3.69 ± 0.14 |
| `Stack (1000000)` | 2.069 ± 0.051 | 1.976 | 2.188 | 3.93 ± 0.16 |

<!--
IMPL={impl} BENCH=Add_pop ROUNDS=20 LENGTH=1000000 dune exec ./bin/bench.flambda.exe
-->
