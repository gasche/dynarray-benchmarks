#!/bin/bash
set -x

eval $(opam env --switch=5.1.0 --set-switch)
dune clean
dune build --profile release
cp -f _build/default/bench.exe bin/bench.clambda.exe

eval $(opam env --switch=5.1.1+flambda --set-switch)
dune clean
dune build --profile release
cp -f _build/default/bench.exe bin/bench.flambda.exe
