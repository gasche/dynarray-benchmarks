#!/bin/bash

function run {
  local BENCH=$1
  local COEFF=$2
  local IMPLS=$3
  echo "# $BENCH ($IMPLS)"
  for LENGTH in 10 1000 $(( 1000 * 1000 ))
  do
      ROUNDS=$(( $COEFF * 20 * 1000 * 1000 / $LENGTH ))
      echo "## $BENCH; Length: $LENGTH, Rounds: $ROUNDS"
      for COMP in flambda
      do
          echo "### $BENCH ($LENGTH); Compiler: $COMP"
          local FILE=results/bench.$BENCH.$LENGTH.$COMP.md
          local CMD="IMPL={impl} BENCH=$BENCH ROUNDS=$ROUNDS LENGTH=$LENGTH dune exec ./bin/bench.$COMP.exe"
          hyperfine \
              --sort mean-time \
              --warmup 20 --runs 50 \
              -L impl $IMPLS \
              --export-markdown $FILE \
              --command-name "{impl} ($LENGTH)" \
              "$CMD"
          echo
          echo >> $FILE
          echo "<!--" >> $FILE
          echo $CMD >> $FILE
          echo "-->" >> $FILE
      done
  done
}


# Random access benchmarks
#   - we use larger round coefficients because
#     these operations are very fast

IMPLS=Array,CCVector,Base_stack,Dynarray_boxed,Dynarray_unboxed
run Get 5 $IMPLS
run Set 2 $IMPLS

# Add/remove individual elements
IMPLS=Stack,Queue,CCVector,Base_stack,Dynarray_boxed,Dynarray_unboxed
run Add 1 $IMPLS
run Add_pop 1 $IMPLS

# # Bulk traversals
# #   - we did not bother implementing the Base_stack version of several of these,
# #     so it is left out of IMPLS by default
# #   - on large size these benchmarks spend a lot of time allocating memory,
# #     so the system time is large; this is an unavoidable part of the workload
# IMPLS=Stack,Queue,CCVector,Dynarray_boxed,Dynarray_unboxed
# run Iter 1 $IMPLS,Base_stack
# run To_array 2 $IMPLS
# run Of_array 2 $IMPLS
# run Of_list 2 $IMPLS,Base_stack
# run Append_array 2 $IMPLS
