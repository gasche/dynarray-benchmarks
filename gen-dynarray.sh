#!/bin/bash

GIT=~/Prog/ocaml/github-trunk

function gen {
  SUFFIX=$1
  REF=$2
  EXT=$3

  rm -f dynarray_$SUFFIX.$EXT
  echo "(*" >> dynarray_$SUFFIX.$EXT
  (cd $GIT ; git log $REF -- stdlib/dynarray.$EXT | head -n 1) >> dynarray_$SUFFIX.$EXT
  echo "*)" >> dynarray_$SUFFIX.$EXT
  echo >> dynarray_$SUFFIX.$EXT
  (cd $GIT ; git show $REF:stdlib/dynarray.$EXT) >> dynarray_$SUFFIX.$EXT
}

gen boxed 5.2 ml
gen boxed 5.2 mli

gen unboxed dynarray-unboxed-dummy ml
gen unboxed dynarray-unboxed-dummy mli
