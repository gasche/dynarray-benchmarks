The code in this directory (and ../base_stack.ml) is extracted from
the Base implementation, version v0.17~preview.128.37+01

  https://github.com/janestreet/base/tree/436bc5f284f55dcc99fd34cb772dd2ce0482b83d/src

We removed the parts that are not necessary to compile the Stack
module, and added implementations of the `get` and `set` random-access
functions in Stack.

