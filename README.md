This repository contains micro-benchmarks written to evaluate the
performance of the Dynarray module of the OCaml standard library.

We compare performance with the `Array`, `Stack` and `Queue` modules
of the standard library, `CCVector` from
[Containers](https://github.com/c-cube/ocaml-containers/) and the
`Stack` module of the [Base](https://github.com/janestreet/base/)
library, which is built on top of a dynamic array. The implementations
of `Dynarray`, `CCVector` and `Base_stack` are copied/vendored in this
repository, and were last updated in early January 2024.
